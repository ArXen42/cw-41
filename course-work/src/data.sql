INSERT INTO geopela.courses (course_id, course_title, is_course_visible) VALUES ('db', 'БД', 1);
INSERT INTO geopela.courses (course_id, course_title, is_course_visible) VALUES ('la', 'Линейная алгебра', 1);
INSERT INTO geopela.courses (course_id, course_title, is_course_visible) VALUES ('oblivion', 'Забытый курс', 0);
INSERT INTO geopela.courses (course_id, course_title, is_course_visible) VALUES ('empty', 'Пустой курс с преподавателем', 1);
INSERT INTO geopela.courses (course_id, course_title, is_course_visible) VALUES ('hidden', 'Забытый курс с оставшимся подписчиком', 0);

INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('student2@openmailbox.org', '2', 'Студент', '2', NULL, 'student3');
INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('dbteacher@openmailbox.org', '42', 'Преподаватель', '0', NULL, 'db_teacher');
INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('student1@openmailbox.org', '0', 'Студент', '0', NULL, 'student1');
INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('student2@openmailbox.org', '1', 'Студент', '1', NULL, 'student2');
INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('student3@openmailbox.org', '3', 'Студент', '3', NULL, 'student4');
INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('student4@openmailbox.org', '4', 'Студент', '4', NULL, 'student5');
INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('expelled-student@openmailbox.org', '5', 'Студент', 'Отчисленный', NULL, 'expelled');
INSERT INTO geopela.users (email, user_id, first_name, last_name, middle_name, login)
VALUES ('miscteacher@openmailbox.org', '43', 'Преподаватель', '1', NULL, 'misc_teacher');

INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('2', 'la', 'student');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('42', 'db', 'teacher');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('0', 'db', 'student');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('1', 'db', 'listener');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('1', 'la', 'student');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('42', 'la', 'curator');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('5', 'hidden', 'student');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('43', 'empty', 'teacher');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('3', 'db', 'student');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('3', 'la', 'student');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('4', 'la', 'student');
INSERT INTO geopela.users_courses (user_id, course_id, user_role) VALUES ('4', 'db', 'listener');

INSERT INTO geopela.course_modules (course_id, is_module_visible, module_title, module_id, module_type)
VALUES ('db', 1, 'Тест по нормализации', 'db_test0', 'test');
INSERT INTO geopela.course_modules (course_id, is_module_visible, module_title, module_id, module_type)
VALUES ('db', 1, 'Курсовая работа', 'db_ex0', 'exercise');
INSERT INTO geopela.course_modules (course_id, is_module_visible, module_title, module_id, module_type)
VALUES ('la', 1, 'Расчетная работа по линейным операторам', 'la_ex0', 'exercise');
INSERT INTO geopela.course_modules (course_id, is_module_visible, module_title, module_id, module_type)
VALUES ('db', 1, 'Тест по SQL', 'db_test1', 'test');
INSERT INTO geopela.course_modules (course_id, is_module_visible, module_title, module_id, module_type)
VALUES ('db', 0, 'Скрытый файл', 'db_file0', 'file');

INSERT INTO geopela.exercises_modules (exercise_text, exercise_opening_date, module_id)
VALUES ('some text', TO_DATE('2015-05-17 18:27:12', 'YYYY-MM-DD HH24:MI:SS'), 'la_ex0');
INSERT INTO geopela.exercises_modules (exercise_text, exercise_opening_date, module_id)
VALUES ('Загрузите курсовую работу до 22 мая', TO_DATE('2017-05-16 15:13:46', 'YYYY-MM-DD HH24:MI:SS'), 'db_ex0');

INSERT INTO geopela.exercises_user_answers (answer_number, user_id, answer_file_link, answer_date, review_text, review_date, exercise_scores, module_id)
VALUES (3, '0', 'https://files.org/db_ex0_user0_f3', TO_DATE('2017-05-21 14:34:19', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL, 'db_ex0');
INSERT INTO geopela.exercises_user_answers (answer_number, user_id, answer_file_link, answer_date, review_text, review_date, exercise_scores, module_id)
VALUES (0, '0', 'https://files.org/db_ex0_user0_f0', TO_DATE('2017-05-17 15:18:28', 'YYYY-MM-DD HH24:MI:SS'), 'Плохо',
        TO_DATE('2017-05-17 21:18:39', 'YYYY-MM-DD HH24:MI:SS'), 0, 'db_ex0');
INSERT INTO geopela.exercises_user_answers (answer_number, user_id, answer_file_link, answer_date, review_text, review_date, exercise_scores, module_id)
VALUES (1, '0', 'https://files.org/db_ex0_user0_f1', TO_DATE('2017-05-19 19:14:30', 'YYYY-MM-DD HH24:MI:SS'), 'Немногим лучше',
        TO_DATE('2017-05-20 15:22:01', 'YYYY-MM-DD HH24:MI:SS'), 10, 'db_ex0');
INSERT INTO geopela.exercises_user_answers (answer_number, user_id, answer_file_link, answer_date, review_text, review_date, exercise_scores, module_id)
VALUES (2, '0', 'https://files.org/db_ex0_user0_f2', TO_DATE('2017-05-20 15:22:29', 'YYYY-MM-DD HH24:MI:SS'), 'Все еще не то',
        TO_DATE('2017-05-20 23:33:57', 'YYYY-MM-DD HH24:MI:SS'), 50, 'db_ex0');
INSERT INTO geopela.exercises_user_answers (answer_number, user_id, answer_file_link, answer_date, review_text, review_date, exercise_scores, module_id)
VALUES (0, '1', 'https://files.org/la_ex0_user1_f0', TO_DATE('2015-06-17 18:27:52', 'YYYY-MM-DD HH24:MI:SS'), NULL, NULL, NULL, 'la_ex0');

INSERT INTO geopela.files_modules (file_description, file_link, module_id) VALUES ('Hidden file', 'secret', 'db_file0');

INSERT INTO geopela.tests_modules (test_opening_date, duration_in_seconds, module_id)
VALUES (TO_DATE('2017-05-17 14:45:33', 'YYYY-MM-DD HH24:MI:SS'), 2400, 'db_test0');
INSERT INTO geopela.tests_modules (test_opening_date, duration_in_seconds, module_id)
VALUES (TO_DATE('2017-04-09 00:11:19', 'YYYY-MM-DD HH24:MI:SS'), 2000, 'db_test1');

INSERT INTO geopela.tests_questions (question_id, question_text, module_id) VALUES ('dbtest00', 'Вопрос 0', 'db_test0');
INSERT INTO geopela.tests_questions (question_id, question_text, module_id) VALUES ('dbtest01', 'Вопрос 1', 'db_test0');
INSERT INTO geopela.tests_questions (question_id, question_text, module_id) VALUES ('dbtest10', 'Вопрос 0', 'db_test1');
INSERT INTO geopela.tests_questions (question_id, question_text, module_id) VALUES ('dbtest11', 'Вопрос 1', 'db_test1');

INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest00', 'Правильный ответ', 'dbt000', 50);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest00', 'Неправильный ответ', 'dbt001', 0);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest01', 'Правильный ответ', 'dbt010', 100);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest01', 'Частично правильный ответ', 'dbt011', 50);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest01', 'Неправильный ответ', 'dbt012', 0);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest10', 'Неправильный ответ', 'dbt100', 0);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest10', 'Правильныый ответ', 'dbt101', 100);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest11', 'Неправильнй ответ', 'dbt110', 0);
INSERT INTO geopela.tests_answers (question_id, answer_text, answer_id, answer_scores) VALUES ('dbtest11', 'Ответ с ошибкой в баллах', 'dbt111', 1);

INSERT INTO geopela.tests_user_attempts (attempt_id, user_id) VALUES ('0dbt00', '0');
INSERT INTO geopela.tests_user_attempts (attempt_id, user_id) VALUES ('0dbt01', '0');

INSERT INTO geopela.tests_user_answers (attempt_id, answer_id) VALUES ('0dbt00', 'dbt001');
INSERT INTO geopela.tests_user_answers (attempt_id, answer_id) VALUES ('0dbt00', 'dbt011');
INSERT INTO geopela.tests_user_answers (attempt_id, answer_id) VALUES ('0dbt01', 'dbt000');
INSERT INTO geopela.tests_user_answers (attempt_id, answer_id) VALUES ('0dbt01', 'dbt010');


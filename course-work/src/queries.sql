--1
SELECT course_title
FROM courses
	INNER JOIN users_courses USING (course_id)
GROUP BY course_id, course_title, is_course_visible
HAVING is_course_visible = 0;

--2
SELECT
	first_name,
	last_name,
	middle_name,
	module_title,
	module_type,
	scores
FROM
	(SELECT
		 user_id,
		 module_id,
		 NVL(MAX(attempt_scores), 0) AS scores
	 FROM
		 (SELECT
			  user_id,
			  module_id,
			  SUM(answer_scores) AS attempt_scores
		  FROM tests_user_attempts
			  INNER JOIN tests_user_answers USING (attempt_id)
			  INNER JOIN tests_answers USING (answer_id)
			  INNER JOIN tests_questions USING (question_id)
		  GROUP BY attempt_id, user_id, module_id)
	 GROUP BY user_id, module_id

	 UNION

	 SELECT
		 user_id,
		 module_id,
		 NVL(MAX(exercise_scores), 0) AS scores
	 FROM exercises_user_answers
		 INNER JOIN exercises_modules USING (module_id)
	 GROUP BY user_id, module_id)
	INNER JOIN users USING (user_id)
	INNER JOIN course_modules USING (module_id)
WHERE user_id = &target_user_id;

-- 3
SELECT
	course_title,
	module_title,
	question_text,
	answer_text,
	answer_scores
FROM tests_modules
	INNER JOIN course_modules USING (module_id)
	INNER JOIN courses USING (course_id)
	INNER JOIN tests_questions USING (module_id)
	INNER JOIN tests_answers USING (question_id)
WHERE course_id = &target_course_id
ORDER BY module_id, question_id, answer_scores;

-- 4
SELECT
	first_name,
	last_name,
	middle_name,
	course_title,
	module_title,
	exercise_text,
	answer_file_link
FROM exercises_user_answers
	INNER JOIN users USING (user_id)
	INNER JOIN course_modules USING (module_id)
	INNER JOIN exercises_modules USING (module_id)
	INNER JOIN courses USING (course_id)
WHERE user_id = &target_user_id
ORDER BY answer_date;

-- 5
SELECT
	user_id,
	first_name,
	last_name,
	middle_name
FROM users_courses
	INNER JOIN users USING (user_id)
WHERE user_role = 'teacher';

-- 6
SELECT user_id
FROM
	(SELECT
		 user_id,
		 count(*) AS all_tasks
	 FROM
		 (SELECT user_id
		  FROM users_courses
			  INNER JOIN course_modules USING (course_id)
		  WHERE user_role = 'student'
		        AND module_type IN ('test', 'exercise'))
	 GROUP BY user_id)
	LEFT OUTER JOIN
	(SELECT
		 user_id,
		 count(*) AS sumbitted_tasks
	 FROM (SELECT
		       user_id,
		       module_id
	       FROM exercises_user_answers
	       GROUP BY user_id, module_id
	       UNION
	       SELECT
		       user_id,
		       module_id
	       FROM tests_user_attempts
		       INNER JOIN tests_user_answers USING (attempt_id)
		       INNER JOIN tests_answers USING (answer_id)
		       INNER JOIN tests_questions USING (question_id)
	       GROUP BY user_id, module_id)
	 GROUP BY user_id)
	USING (user_id)
WHERE all_tasks - nvl(sumbitted_tasks, 0) > all_tasks / 2;

-- 7
SELECT
	user_id,
	course_id
FROM users_courses
	LEFT OUTER JOIN course_modules USING (course_id)
	LEFT OUTER JOIN tests_modules USING (module_id)
GROUP BY user_id, course_id, user_role
HAVING (user_role = 'teacher')
       AND (COUNT(test_opening_date) = 0));

--8
SELECT
	course_id,
	course_title
FROM courses
	INNER JOIN course_modules USING (course_id)
	INNER JOIN exercises_user_answers USING (module_id)
WHERE review_date IS NULL
      AND (SYSDATE - answer_date > 7)
      AND course_id != ANY (SELECT course_id
                            FROM users_courses
                            GROUP BY course_id, user_role
                            HAVING user_role = 'teacher');

--9
SELECT
	course_id,
	(SELECT COUNT(*)
	 FROM users_courses
	 WHERE users_courses.course_id = courses.course_id
	       AND user_role = 'student')     AS students_count,
	(SELECT COUNT(*)
	 FROM users_courses
	 WHERE users_courses.course_id = courses.course_id
	       AND user_role = 'teacher')     AS teachers_count,
	(SELECT COUNT(*)
	 FROM users_courses
	 WHERE users_courses.course_id = courses.course_id
	       AND user_role = 'curator')     AS curators_count,
	(SELECT COUNT(DISTINCT module_id)
	 FROM course_modules
		 INNER JOIN exercises_modules USING (module_id)
		 INNER JOIN exercises_user_answers USING (module_id)
	 WHERE courses.course_id = course_id) AS exercises_count,
	(SELECT MAX(answer_date)
	 FROM course_modules
		 INNER JOIN exercises_modules USING (module_id)
		 INNER JOIN exercises_user_answers USING (module_id)
	 WHERE courses.course_id = course_id) AS latest_exercise_date
FROM courses;

--10
SELECT
	course_title,
	module_title,
	question_text,
	scores_sum
FROM
	(SELECT
		 question_id,
		 sum(answer_scores) AS scores_sum
	 FROM tests_questions
		 INNER JOIN tests_answers USING (question_id)
	 GROUP BY question_id
	 HAVING SUM(answer_scores) < 100)
	INNER JOIN tests_questions USING (question_id)
	INNER JOIN course_modules USING (module_id)
	INNER JOIN courses USING (course_id);

--10 - mod
SELECT
	course_id,
	module_id,
	question_id,
	sum(answer_scores) AS scores_sum
FROM tests_questions
	INNER JOIN tests_answers USING (question_id)
	INNER JOIN course_modules USING (module_id)
	INNER JOIN courses USING (course_id)
GROUP BY question_id, course_id, module_id
HAVING SUM(answer_scores) < 100;

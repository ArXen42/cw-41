#!/bin/bash

for file in *.svg
do
 			filename=${file%.svg}
			echo $filename
			inkscape -D -z --file=$file --export-pdf=../pdf/$filename'.pdf'
done

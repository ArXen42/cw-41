
CREATE TABLE actions_history
(
	user_id              VARCHAR2(8) NOT NULL ,
	action_time          DATE NOT NULL ,
	action_description   CLOB NOT NULL 
);



CREATE UNIQUE INDEX XPKactions_history ON actions_history
(user_id   ASC,action_time   ASC);



ALTER TABLE actions_history
	ADD  PRIMARY KEY (user_id,action_time);



CREATE TABLE course_modules
(
	course_id            VARCHAR2(8) NOT NULL ,
	is_module_visible    NUMBER(1) NOT NULL  CHECK (is_module_visible IN (0, 1)),
	module_title         VARCHAR2(1024) NOT NULL ,
	module_id            VARCHAR2(8) NOT NULL ,
	module_type          VARCHAR2(20) NOT NULL  CHECK (module_type IN ('test', 'exercise', 'lection', 'file'))
);



CREATE UNIQUE INDEX XPKcourse_modules ON course_modules
(module_id   ASC);



ALTER TABLE course_modules
	ADD  PRIMARY KEY (module_id);




ALTER TABLE course_modules
	MODIFY is_module_visible DEFAULT 0;



CREATE TABLE courses
(
	course_id            VARCHAR2(8) NOT NULL ,
	course_title         VARCHAR2(1024) NOT NULL ,
	is_course_visible    NUMBER(1) NOT NULL  CHECK (is_course_visible IN (0, 1))
);



CREATE UNIQUE INDEX XPKcourses ON courses
(course_id   ASC);



ALTER TABLE courses
	ADD  PRIMARY KEY (course_id);




ALTER TABLE courses
	MODIFY is_course_visible DEFAULT 0;



CREATE TABLE exercises_modules
(
	exercise_text        CLOB NOT NULL ,
	exercise_opening_date DATE NOT NULL ,
	module_id            VARCHAR2(8) NOT NULL 
);



CREATE UNIQUE INDEX XPKexercises_modules ON exercises_modules
(module_id   ASC);



ALTER TABLE exercises_modules
	ADD  PRIMARY KEY (module_id);



CREATE TABLE exercises_user_answers
(
	answer_number        NUMBER(2) NOT NULL ,
	user_id              VARCHAR2(8) NOT NULL ,
	answer_file_link     VARCHAR2(1024) NOT NULL ,
	answer_date          DATE NOT NULL ,
	review_text          CLOB NULL ,
	review_date          DATE NULL ,
	exercise_scores      NUMBER(3) NULL ,
	module_id            VARCHAR2(8) NOT NULL ,
CHECK ( answer_date < review_date )
);



CREATE UNIQUE INDEX XPKexercise_user_answers ON exercises_user_answers
(answer_number   ASC,user_id   ASC,module_id   ASC);



ALTER TABLE exercises_user_answers
	ADD  PRIMARY KEY (answer_number,user_id,module_id);



CREATE TABLE files_modules
(
	file_description     CLOB NOT NULL ,
	file_link            VARCHAR2(1024) NOT NULL ,
	module_id            VARCHAR2(8) NOT NULL 
);



CREATE UNIQUE INDEX XPKfiles_modules ON files_modules
(module_id   ASC);



ALTER TABLE files_modules
	ADD  PRIMARY KEY (module_id);



CREATE TABLE lections_modules
(
	lecture_text         CLOB NOT NULL ,
	module_id            VARCHAR2(8) NOT NULL 
);



CREATE UNIQUE INDEX XPKlections_modules ON lections_modules
(module_id   ASC);



ALTER TABLE lections_modules
	ADD  PRIMARY KEY (module_id);



CREATE TABLE salts_hashes
(
	user_id              VARCHAR2(8) NOT NULL ,
	salt                 RAW(32) NOT NULL ,
	hash                 RAW(256) NOT NULL 
);



CREATE UNIQUE INDEX XPKsalts_hashes ON salts_hashes
(user_id   ASC);



ALTER TABLE salts_hashes
	ADD  PRIMARY KEY (user_id);



CREATE TABLE tests_answers
(
	question_id          VARCHAR2(8) NOT NULL ,
	answer_text          CLOB NOT NULL ,
	answer_id            VARCHAR2(8) NOT NULL ,
	answer_scores        NUMBER(3) NOT NULL  CHECK (answer_scores BETWEEN 0 AND 100)
);



CREATE UNIQUE INDEX XPKtests_questions_answers ON tests_answers
(answer_id   ASC);



ALTER TABLE tests_answers
	ADD  PRIMARY KEY (answer_id);



CREATE TABLE tests_modules
(
	test_opening_date    DATE NOT NULL ,
	duration_in_seconds  INTEGER NOT NULL ,
	module_id            VARCHAR2(8) NOT NULL 
);



CREATE UNIQUE INDEX XPKtests_modules ON tests_modules
(module_id   ASC);



ALTER TABLE tests_modules
	ADD  PRIMARY KEY (module_id);



CREATE TABLE tests_questions
(
	question_id          VARCHAR2(8) NOT NULL ,
	question_text        CLOB NOT NULL ,
	module_id            VARCHAR2(8) NOT NULL 
);



CREATE UNIQUE INDEX XPKtests_questions ON tests_questions
(question_id   ASC);



ALTER TABLE tests_questions
	ADD  PRIMARY KEY (question_id);



CREATE TABLE tests_user_answers
(
	attempt_id           VARCHAR2(8) NOT NULL ,
	answer_id            VARCHAR2(8) NOT NULL 
);



CREATE UNIQUE INDEX XPKtests_user_answers ON tests_user_answers
(attempt_id   ASC,answer_id   ASC);



ALTER TABLE tests_user_answers
	ADD  PRIMARY KEY (attempt_id,answer_id);



CREATE TABLE tests_user_attempts
(
	attempt_id           VARCHAR2(8) NOT NULL ,
	user_id              VARCHAR2(8) NOT NULL 
);



CREATE UNIQUE INDEX XPKtests_user_attempts ON tests_user_attempts
(attempt_id   ASC);



ALTER TABLE tests_user_attempts
	ADD  PRIMARY KEY (attempt_id);



CREATE TABLE users
(
	email                VARCHAR2(254) NOT NULL ,
	user_id              VARCHAR2(8) NOT NULL ,
	first_name           VARCHAR2(128) NOT NULL ,
	last_name            VARCHAR2(128) NOT NULL ,
	middle_name          VARCHAR2(128) NULL ,
	login                VARCHAR2(40) NOT NULL 
);



CREATE UNIQUE INDEX XPKusers ON users
(user_id   ASC);



ALTER TABLE users
	ADD  PRIMARY KEY (user_id);



CREATE UNIQUE INDEX XAK1users ON users
(login   ASC);



ALTER TABLE users
ADD  UNIQUE (login);



CREATE TABLE users_courses
(
	user_id              VARCHAR2(8) NOT NULL ,
	course_id            VARCHAR2(8) NOT NULL ,
	user_role            VARCHAR2(20) NOT NULL  CHECK (user_role IN ('teacher', 'curator', 'assistant', 'student', 'listener'))
);



CREATE UNIQUE INDEX XPKusers_courses ON users_courses
(user_id   ASC,course_id   ASC);



ALTER TABLE users_courses
	ADD  PRIMARY KEY (user_id,course_id);



CREATE  VIEW courses_pending_teacher ( course_id,course_title ) 
	 AS
SELECT
	course_id,
	course_title
FROM
	(SELECT course_id
	 FROM courses
	 WHERE course_id != ANY (SELECT course_id
	                         FROM users_courses
	                         GROUP BY course_id, user_role
	                         HAVING user_role = 'teacher'))
	INNER JOIN course_modules USING (course_id)
	INNER JOIN exercises_user_answers USING (module_id)
	INNER JOIN courses USING (course_id)
WHERE review_date IS NULL AND (SYSDATE - answer_date > 7);



ALTER TABLE actions_history
	ADD (FOREIGN KEY (user_id) REFERENCES users (user_id));



ALTER TABLE course_modules
	ADD (FOREIGN KEY (course_id) REFERENCES courses (course_id) ON DELETE CASCADE);



ALTER TABLE exercises_modules
	ADD (FOREIGN KEY (module_id) REFERENCES course_modules(module_id) ON DELETE CASCADE);



ALTER TABLE exercises_user_answers
	ADD (FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE);



ALTER TABLE exercises_user_answers
	ADD (FOREIGN KEY (module_id) REFERENCES exercises_modules (module_id) ON DELETE CASCADE);



ALTER TABLE files_modules
	ADD (FOREIGN KEY (module_id) REFERENCES course_modules(module_id) ON DELETE CASCADE);



ALTER TABLE lections_modules
	ADD (FOREIGN KEY (module_id) REFERENCES course_modules(module_id) ON DELETE CASCADE);



ALTER TABLE salts_hashes
	ADD (FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE);



ALTER TABLE tests_answers
	ADD (FOREIGN KEY (question_id) REFERENCES tests_questions (question_id) ON DELETE CASCADE);



ALTER TABLE tests_modules
	ADD (FOREIGN KEY (module_id) REFERENCES course_modules(module_id) ON DELETE CASCADE);



ALTER TABLE tests_questions
	ADD (FOREIGN KEY (module_id) REFERENCES tests_modules (module_id) ON DELETE CASCADE);



ALTER TABLE tests_user_answers
	ADD (FOREIGN KEY (attempt_id) REFERENCES tests_user_attempts (attempt_id) ON DELETE CASCADE);



ALTER TABLE tests_user_answers
	ADD (FOREIGN KEY (answer_id) REFERENCES tests_answers (answer_id) ON DELETE CASCADE);



ALTER TABLE tests_user_attempts
	ADD (FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE);



ALTER TABLE users_courses
	ADD (FOREIGN KEY (user_id) REFERENCES users (user_id) ON DELETE CASCADE);



ALTER TABLE users_courses
	ADD (FOREIGN KEY (course_id) REFERENCES courses (course_id) ON DELETE CASCADE);


